# Die Olympischen Spiele 2204

The corresponding geocache is: [GC3Y3Z1](https://www.geocaching.com/geocache/GC3Y3Z1_olympische-spiele-2204?guid=49d333d3-c45e-4abd-8e36-bf4a2b410403)

The corresponding cartridge on wherigo.com is: [Die Olympischen Spiele 2204](http://www.wherigo.com/cartridge/details.aspx?CGUID=d0e26c93-fc44-4389-ad11-38cfdd6cfa22)

This project is developed with [Urwigo](https://urwigo.cz/index.php).

The media files are in the same folder because for Urwigo is this the best case.