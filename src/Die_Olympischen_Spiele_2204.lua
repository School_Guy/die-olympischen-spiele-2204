require "global"
require "messages"

-- Language ready flags
de_ready = true
en_ready = false
fr_ready = false
es_ready = false
-- Language ready flags

function catridgeStart()
    if (V_Sprache == "de") then
        welcome()
    else
        msgMitNachrichtUndBild("I am sorry that I am currently not able to offer your language." .. cr ..
            "Please contact me under matrixfueller@gmail.com if you can help me translate it into" ..
            " your language.", P_Signal_Achtung,
        function()
            Wherigo.ShowScreen(Wherigo.MAINSCREEN)
        end)
    end
end

function welcome()
    msgMitNachrichtUndBild(getMessage(V_Sprache, 1), P_Signal_Achtung,
        function()
            msgMitNachrichtUndBild(getMessage(V_Sprache, 2), P_Signal_weibl,
                function()
                    Sportstaette_01.Active = true
                    Sportstaette_01.Display = true
                    Wherigo.ShowScreen(Wherigo.MAINSCREEN)
                end)
        end)
end

function Sportstaette_02_OnEnter()
    msgMitNachricht(getMessage(V_Sprache, 3),
        function()
            msgMitNachrichtUndBild(getMessage(V_Sprache, 4), P_Signal_Fragezeichen,
                function()
                    I_Badehose.Commands.Cmd01.Enabled = false
                    I_Badehose.Commands.Cmd02.Enabled = true
                    C_Trainerin.Commands.Cmd02.Enabled = true
                    Save()
                    Wherigo.ShowScreen(Wherigo.MAINSCREEN)
                end)
        end)
end

function Sportstaette_03_OnEnter()
    msgMitNachrichtUndBild(getMessage(V_Sprache, 5), P_Glueckwunsch,
        function()
            msgMitNachricht(getMessage(V_Sprache, 6),
                function()
                    C_Trainerin.Commands.Cmd02.Enabled = false
                    C_Trainerin:MoveTo(Sportstaette_03)
                    C_Trainerin.Commands.Cmd03.Enabled = true
                    I_Kanadier.Commands.Cmd01.Enabled = false
                    I_Kanadier.Commands.Cmd02.Enabled = true
                    I_Paddel.Commands.Cmd01.Enabled = true
                    I_Paddel.Commands.Cmd02.Enabeld = false
                    I_Paddel.Commands.Cmd02.Enabled = false
                    Wherigo.ShowScreen(Wherigo.MAINSCREEN)
                end)
        end)
end

function Sportstaette_04_OnEnter()
    msgMitNachricht(getMessage(V_Sprache, 9),
        function()
            msgMitNachricht(getMessage(V_Sprache, 10),
                function()
                    msgMitNachricht(getMessage(V_Sprache, 11),
                        function()
                            C_Trainerin.Commands.Cmd03.Enabled = false
                            C_Trainerin:MoveTo(Sportstaette_04)
                            C_Trainerin.Commands.Cmd04.Enabled = true
                            Wherigo.GetInput(Q_Stage_4)
                        end)
                end)
        end)
end

function Sportstaette_05_OnEnter()
    msgMitNachricht(getMessage(V_Sprache, 12),
        function()
            msgMitNachricht(getMessage(V_Sprache, 13),
                function()
                    msgMitNachricht(getMessage(V_Sprache, 14),
                        function()
                            I_Restzeit:MoveTo(Player)
                            Sportstaette_06.Active = true
                            Cd_Stage_56:Start()
                        end)
                end)
        end)
end

function Sportstaette_96_OnEnter()
    msgMitNachricht(getMessage(V_Sprache, 37),
        function()
            Cd_Stage_56_long:Start()
        end)
end

function Sportstaette_06_OnEnter()
    Cd_Stage_56:Stop()
    Cd_Stage_56_long:Stop()
    Sportstaette_05.Active = false
    msgMitNachrichtUndBild(getMessage(V_Sprache, 15), P_Signal_Thumbs_Up,
        function()
            msgMitNachricht(getMessage(V_Sprache, 16),
                function()
                    C_Trainerin.Commands.Cmd04.Enabled = false
                    C_Trainerin:MoveTo(Sportstaette_06)
                    C_Trainerin.Commands.Cmd05.Enabled = true
                    I_Rennrad.Commands.Cmd01.Enabled = false
                    I_Rennrad.Commands.Cmd02.Enabled = true
                    Wherigo.GetInput(Q_Stage_6)
                end)
        end)
end

function Sportstaette_07_OnEnter()
    msgMitNachrichtUndBild(getMessage(V_Sprache, 17), P_Signal_Fahrrad_Fans,
        function()
            msgMitNachricht(getMessage(V_Sprache, 18),
                function()
                    C_Trainerin.Commands.Cmd05.Enabled = false
                    C_Trainerin:MoveTo(Sportstaette_07)
                    C_Trainerin.Commands.Cmd06.Enabled = true
                    I_Kugel.Commands.Cmd01.Enabled = false
                    I_Kugel.Commands.Cmd02.Enabled = true
                    Wherigo.ShowScreen(Wherigo.MAINSCREEN)
                end)
        end)
end

function Sportstaette_08_OnEnter()
    msgMitNachrichtUndBild(getMessage(V_Sprache, 19), P_Kugelstossen,
        function()
            msgMitNachricht(getMessage(V_Sprache, 20),
                function()
                    Sportstaette_08.Active = false
                    Sportstaette_09.Active = true
                    Wherigo.ShowScreen(Wherigo.MAINSCREEN)
                end)
        end)
end

function Sportstaette_09_OnEnter()
    msgMitNachricht(getMessage(V_Sprache, 21),
        function()
            msgMitNachrichtUndBild(getMessage(V_Sprache, 22), P_Signal_Fragezeichen,
                function()
                    C_Trainerin.Commands.Cmd06.Enabled = false
                    C_Trainerin:MoveTo(Sportstaette_09)
                    C_Trainerin.Commands.Cmd07.Enabled = true
                    I_Basketball.Commands.Cmd01.Enabled = true
                    I_Basketball.Commands.Cmd02.Enabled = false
                    Wherigo.ShowScreen(Wherigo.MAINSCREEN)
                end)
        end)
end

function Sportstaette_10_OnEnter()
    msgMitNachrichtUndBild(getMessage(V_Sprache, 23), P_Spielstand,
        function()
            msgMitNachricht(getMessage(V_Sprache, 24),
                function()
                    C_Trainerin.Commands.Cmd07.Enabled = false
                    C_Trainerin:MoveTo(Sportstaette_10)
                    C_Trainerin.Commands.Cmd08.Enabled = true
                    I_Schwarzer_Guertel.Commands.Cmd01.Enabled = false
                    I_Schwarzer_Guertel.Commands.Cmd02.Enabled = true
                    Wherigo.ShowScreen(Wherigo.MAINSCREEN)
                end)
        end)
end

function Sportstaette_11_OnEnter()
    msgMitNachrichtUndBild(getMessage(V_Sprache, 24), P_Signal_Judo_mit_Gegner,
        function()
            msgMitNachrichtUndBild(getMessage(V_Sprache, 25), P_Signal_Fragezeichen,
                function()
                    C_Trainerin.Commands.Cmd08.Enabled = false
                    C_Trainerin:MoveTo(Sportstaette_11)
                    C_Trainerin.Commands.Cmd09.Enabled = true
                    I_Pferd.Commands.Cmd01.Enabled = true
                    I_Pferd.Commands.Cmd02.Enabled = false
                    Wherigo.ShowScreen(Wherigo.MAINSCREEN)
                end)
        end)
end

function Sportstaette_12_OnEnter()
    Sportstaette_12.Display = true
    msgMitNachrichtUndBild(getMessage(V_Sprache, 27), P_Signal_Pferd_herunterfallend,
        function()
            msgMitNachrichtUndBild(getMessage(V_Sprache, 28), P_Krankenwagen,
                function()
                    Cd_Notarzt:Start()
                end)
        end)
end

function Sportstaette_13_OnEnter()
    msgMitNachrichtUndBild(getMessage(V_Sprache, 29), P_Signal_Thumbs_Up,
        function()
            msgMitNachricht(getMessage(V_Sprache, 30),
                function()
                    C_Trainerin.Commands.Cmd09.Enabled = false
                    C_Trainerin:MoveTo(Sportstaette_13)
                    C_Trainerin.Commands.Cmd10.Enabled = true
                    I_Speer.Commands.Cmd01.Enabled = false
                    I_Speer.Commands.Cmd02.Enabled = true
                    Wherigo.ShowScreen(Wherigo.MAINSCREEN)
                end)
        end)
end

function Sportstaette_14_OnEnter()
    msgMitNachricht(getMessage(V_Sprache, 31),
        function()
            msgMitNachricht(getMessage(V_Sprache, 32),
                function()
                    Wherigo.GetInput(Q_Abschlussfrage)
                end)
        end)
end

function Sportstaette_99_OnEnter()
    msgMitNachricht(getMessage(V_Sprache, 33),
        function()
            msgMitNachrichtUndBild(getMessage(V_Sprache, 34), P_Final,
                function()
                    msgMitNachricht(getMessage(V_Sprache, 35),
                        function()
                            I_Unlockcode:MoveTo(Player)
                        end)
                end)
        end)
end

function Sportstaette_98_OnEnter()
    msgMitNachricht(getMessage(V_Sprache, 36),
        function()
            Sportstaette_03.Active = false
            Sportstaette_97.Active = false
            Cd_Verkehrsinsel:Start()
            Wherigo.ShowScreen(Wherigo.MAINSCREEN)
        end)
end

function Sportstaette_97_OnEnter()
    msgMitNachrichtUndBild(getMessage(V_Sprache, 7), P_Signal_traurig,
        function()
            msgMitNachricht(getMessage(V_Sprache, 8),
                function()
                    Sportstaette_04.Active = false
                    Sportstaette_03.Active = false
                    Sportstaette_98.Active = true
                end)
        end)
end

function Sportstaette_03_OnExit()
    if not (I_Kanadier:Contains(Player) and I_Paddel:Contains(Player)) then
        Sportstaette_97.Active = false
        Sportstaette_03.Active = false
        print("Korrekt")
        Save()
    else
        print("Fehler")
    end
end

function Sportstaette_05_OnExit()
    if V_Timer_laeuft == true then
        --Do nothing
    else
        msgMitNachricht(getMessage(V_Sprache, 85),
            function()
                Sportstaette_06.Active = false
            end)
    end
end

function Sportstaette_97_OnExit()
    I_Kanadier:MoveTo(nil)
    I_Kanadier:MoveTo(nil)
    Sportstaette_97.Active = false
end

function Sportstaette_98_OnExit()
    msgMitNachricht(getMessage(V_Sprache, 86),
        function()
            Cd_Verkehrsinsel:Stop()
        end)
end

function Q_Abschlussfrage_Input(input)
    if input == nil then
        input = ""
    end
    if input == "1632" then
        msgMitNachrichtUndBild(getMessage(V_Sprache, 60), P_Signal_freuend,
            function()
                msgMitNachricht(getMessage(V_Sprache, 61),
                    function()
                        Sportstaette_14.Active = false
                        Sportstaette_99.Active = true
                        Wherigo.ShowScreen(Wherigo.MAINSCREEN)
                    end)
            end)
    else
        msgMitNachricht(getMessage(V_Sprache, 62),
            function()
                Wherigo.GetInput(Q_Abschlussfrage)
            end)
    end
end

function Q_Abwurf_Input(input)
    if Wherigo.NoCaseEquals(input, "Skeetschiessen") then
        msgMitNachricht(getMessage(V_Sprache, 63),
            function()
                Sportstaette_12.Active = false
                Sportstaette_13.Active = true
                Wherigo.ShowScreen(Wherigo.MAINSCREEN)
            end)
    elseif Wherigo.NoCaseEquals(input, "Rehe jagen") then
        msgMitNachricht(getMessage(V_Sprache, 64),
            function()
                Wherigo.GetInput(Q_Abwurf)
            end)
    elseif Wherigo.NoCaseEquals(input, "Pferd erschiessen") then
        msgMitNachricht(getMessage(V_Sprache, 65),
            function()
                Wherigo.GetInput(Q_Abwurf)
            end)
    end
end

function Q_Stage_4_Input(input)
    if Wherigo.NoCaseEquals(input, "Wasserball") then
        msgMitNachrichtUndBild(getMessage(V_Sprache, 66), P_Wasserball_real,
            function()
                Sportstaette_04.Active = false
                Sportstaette_05.Active = true
                Wherigo.ShowScreen(Wherigo.MAINSCREEN)
            end)
    else
        msgMitNachricht(getMessage(V_Sprache, 67),
            function()
                Wherigo.GetInput(Q_Stage_4)
            end)
    end
end

function Q_Stage_6_Input(input)
    if input == nil then
        input = ""
    end
    if input == "43" then
        msgMitNachrichtUndBild(getMessage(V_Sprache, 68), P_Albert_Einstein,
            function()
                msgMitNachricht(getMessage(V_Sprache, 9),
                    function()
                        msgMitNachricht(getMessage(V_Sprache, 69),
                            function()
                                --Items
                                Wherigo.ShowScreen(Wherigo.MAINSCREEN)
                            end)
                    end)
            end)
    else
        msgMitNachricht(getMessage(V_Sprache, 71),
            function()
                Wherigo.GetInput(Q_Stage_6_1)
            end)
    end
end

function Q_Stage_6_1_Input(input)
    if input == nil then
        input = ""
    end
    if input == "43" then
        msgMitNachricht(getMessage(V_Sprache, 72),
            function()
                --Items
                Wherigo.ShwoScreen(Wherigo.MAINSCREEN)
            end)
    else
        msgMitNachricht(getMessage(V_Sprache, 73),
            function()
                WHerigo.GetInput(Q_Stage_6_1)
            end)
    end
end

function Q_Stage_8_Input(input)
    if input == nil then
        input = ""
    end
    if input == "416" then
        msgMitNachricht(getMessage(V_Sprache, 74),
            function()
                Cd_Stage_81:Start()
                Wherigo.ShwoScreen(Wherigo.MAINSCREEN)
            end)
    else
        msgMitNachricht(getMessage(V_Sprache, 75),
            function()
                Cd_Stage_82:Start()
                Wherigo.ShwoScreen(Wherigo.MAINSCREEN)
            end)
    end
end

function Cd_Notarzt_OnStart()
    msgMitNachrichtUndBild("", P_Stoppuhr,
        function()
            Wherigo.ShowScreen(Wherigo.MAINSCREEN)
        end)
end

function Cd_Verkehrsinsel_OnStart()
    msgMitNachrichtUndBild(getMessage(V_Sprache, 82), P_Stoppuhr,
        function()
            Wherigo.ShwoScreen(Wherigo.MAINSCREEN)
        end)
end

function Cd_Notarzt_OnElapsed()
    msgMitNachricht(getMessage(V_Sprache, 76),
        function()
            msgMitNachrichtUndBild(getMessage(V_Sprache, 77), P_Gewehr,
                function()
                    msgMitNachricht(getMessage(V_Sprache, 78),
                        function()
                            Wherigo.GetInput(Q_Abwurf)
                        end)
                end)
        end)
end

function Cd_Stage_56_long_OnElapsed()
    msgMitNachricht(getMessage(V_Sprache, 79),
        function()
            Wherigo.ShowScreen(Wherigo.MAINSCREEN)
        end)
end

function Cd_Stage_82_OnElapsed()
    msgMitNachricht(getMessage(V_Sprache, 80),
        function()
            Wherigo.ShowScreen(Wherigo.MAINSCREEN)
        end)
end

function Cd_Stage_81_OnElapsed()
    msgMitNachricht(getMessage(V_Sprache, 80),
        function()
            Wherigo.ShowScreen(Wherigo.MAINSCREEN)
        end)
end

function Cd_Stage_56_OnElapsed()
    if V_Zeit_verstrichen < V_Gesamtzeit then
        V_Zeit_verstrichen = V_Zeit_verstrichen + 1
        V_Restzeit = V_Gesamtzeit - V_Zeit_verstrichen
        I_Restzeit.Name = "Restzeit: " .. V_Restzeit .. "s"
        I_Restzeit.Description = "Restzeit: " .. V_Restzeit .. "s" .. cr .. "Verstrichen: " .. V_Zeit_verstrichen .. "s"
        Cd_Stage_56:Start()
    else
        msgMitNachricht(getMessage(V_Sprache, 81),
            function()
                Sportstaette_96.Active = true
                Sportstaette_06.Active = false
            end)
    end
end

function Cd_Verkehrsinsel_OnElapsed()
    msgMitNachricht(getMessage(V_Sprache, 83),
        function()
            Sportstaette_04.Active = true
            Sportstaette_98.Active = false
            I_Paddel:MoveTo(nil)
            I_Kanadier:MoveTo(nil)
        end)
end

function Badehose()
    msgMitNachrichtUndBild(getMessage(V_Sprache, 38), P_Signal_Fragezeichen,
        function()
            msgMitNachrichtUndBild(getMessage(V_Sprache, 39), P_Signal_Fragezeichen,
                function()
                    I_Badehose:MoveTo(nil)
                    Sportstaette_02.Active = false
                    Sportstaette_03.Active = true
                    Wherigo.ShowScreen(Wherigo.MAINSCREEN)
                end)
        end)
end

function Basketball()
    msgMitNachricht(getMessage(V_Sprache, 40),
        function()
            Sportstaette_09.Active = false
            Sportstaette_10.Active = true
            I_Basketball:MoveTo(nil)
            Wherigo.ShowScreen(Wherigo.MAINSCREEN)
        end)
end

function Kanadier()
    msgMitNachricht(getMessage(V_Sprache, 41),
        function()
            Sportstaette_03.Active = true
            Sportstaette_03.Display = false
            Sportstaette_04.Active = true
            Sportstaette_97.Active = true
            Sportstaette_97.Display = false
            I_Kanadier:MoveTo(nil)
            Wherigo.ShowScreen(Wherigo.MAINSCREEN)
        end)
end

function Kugel()
    msgMitNachrichtUndBild(getMessage(V_Sprache, 42), P_Signal_Kugelstossen,
        function()
            Sportstaette_07.Active = false
            Sportstaette_08.Active = true
            I_Kugel:MoveTo(nil)
            Wherigo.ShowScreen(Wherigo.MAINSCREEN)
        end)
end

function Paddel()
    msgMitNachricht(getMessage(V_Sprache, 43),
        function()
            Sportstaette_03.Active = true
            Sportstaette_03.Display = false
            Sportstaette_04.Active = true
            Sportstaette_97.Active = true
            Sportstaette_97.Display = false
            I_Paddel:MoveTo(nil)
            Wherigo.ShowScreen(Wherigo.MAINSCREEN)
        end)
end

function Pferd()
    msgMitNachricht(getMessage(V_Sprache, 44),
        function()
            Sportstaette_11.Active = false
            Sportstaette_12.Active = true
            I_Pferd:MoveTo(nil)
        end)
end

function Rennrad()
    msgMitNachrichtUndBild(getMessage(V_Sprache, 45), P_Signal_Fahrrad,
        function()
            Sportstaette_06.Active = false
            Sportstaette_07.Active = true
            I_Rennrad:MoveTo(nil)
            Wherigo.ShowScreen(Wherigo.MAINSCREEN)
        end)
end

function SchwarzerGuertel()
    msgMitNachricht(getMessage(V_Sprache, 46),
        function()
            Sportstaette_10.Active = false
            Sportstaette_11.Active = true
            I_Schwarzer_Guertel:MoveTo(nil)
            Wherigo.ShowScreen(Wherigo.MAINSCREEN)
        end)
end

function Speer()
    msgMitNachricht(getMessage(V_Sprache, 47),
        function()
            Sportstaette_13.Active = false
            Sportstaette_14.Active = true
            I_Speer:MoveTo(nil)
            Wherigo.ShowScreen(Wherigo.MAINSCREEN)
        end)
end

function C_Trainerin_Cmd01() --Sportstaette 1
    msgMitNachricht(getMessage(V_Sprache, 49),
        function()
            msgMitNachricht(getMessage(V_Sprache, 50),
                function()
                    msgMitNachrichtUndBild(getMessage(V_Sprache, 51), P_Sportgeraete,
                        function()
                            C_Trainerin.Commands.Cmd01.Enabled = false
                            I_Badehose:MoveTo(Player)
                            I_Speer:MoveTo(Player)
                            I_Schwarzer_Guertel:MoveTo(Player)
                            I_Rennrad:MoveTo(Player)
                            I_Paddel:MoveTo(Player)
                            I_Pferd:MoveTo(Player)
                            I_Kugel:MoveTo(Player)
                            I_Kanadier:MoveTo(Player)
                            I_Basketball:MoveTo(Player)
                            Sportstaette_01.Active = false
                            Sportstaette_02.Active = true
                            C_Trainerin:MoveTo(Sportstaette_02)
                            Wherigo.ShowScreen(Wherigo.MAINSCREEN)
                        end)
                end)
        end)
end

function C_Trainerin_Cmd02() --Sportstaette 2
    msgMitNachrichtUndBild(getMessage(V_Sprache, 52), P_Signal_weibl_Tribuene,
        function()
            C_Trainerin.Commands.Cmd02.Enabled = false
            C_Trainerin:MoveTo(Sportstaette_03)
            Wherigo.ShowScreen(Wherigo.MAINSCREEN)
        end)
end

function C_Trainerin_Cmd03() --Sportstaette 3
    msgMitNachricht(getMessage(V_Sprache, 53),
        function()
            C_Trainerin.Commands.Cmd03.Enabled = false
            Wherigo.ShowScreen(Wherigo.MAINSCREEN)
        end)
end

function C_Trainerin_Cmd04() --Sportstaette 4
    msgMitNachrichtUndBild(getMessage(V_Sprache, 54), P_Signal_Frau,
        function()
            C_Trainerin.Commands.Cmd04.Enabled = false
            Wherigo.ShowScreen(Wherigo.MAINSCREEN)
        end)
end

function C_Trainerin_Cmd05() --Sportstaette 6
    msgMitNachrichtUndBild(getMessage(V_Sprache, 55), P_Banane_1,
        function()
            C_Trainerin.Commands.Cmd05.Enabled = false
            Wherigo.ShowScreen(Wherigo.MAINSCREEN)
        end)
end

function C_Trainerin_Cmd06() --Sportstaette 7
    msgMitNachricht(getMessage(V_Sprache, 56),
        function()
            C_Trainerin.Commands.Cmd06.Enabled = false
            Wherigo.ShowScreen(Wherigo.MAINSCREEN)
        end)
end

function C_Trainerin_Cmd07() --Sportstaette 9
    msgMitNachricht(getMessage(V_Sprache, 57),
        function()
            C_Trainerin.Commands.Cmd07.Enabled = false
            Wherigo.ShowScreen(Wherigo.MAINSCREEN)
        end)
end

function C_Trainerin_Cmd08() --Sportstaette 10
    msgMitNachrichtUndBild(getMessage(V_Sprache, 58), P_Signal_lesend,
        function()
            C_Trainerin.Commands.Cmd08.Enabled = false
            Wherigo.ShowScreen(Wherigo.MAINSCREEN)
        end)
end

function C_Trainerin_Cmd09() --Sportstaette 11
    msgMitNachrichtUndBild("", P_Signal_Hufeisen,
        function()
            C_Trainerin.Commands.Cmd09.Enabled = false
            Wherigo.ShowScreen(Wherigo.MAINSCREEN)
        end)
end

function C_Trainerin_Cmd10() --Sportstaette 13
    msgMitNachricht(getMessage(V_Sprache, 59),
        function()
            C_Trainerin.Commands.Cmd10.Enabled = false
            Wherigo.ShowScreen(Wherigo.MAINSCREEN)
        end)
end

function I_Unlockcode_Abrufen()
    msgMitNachricht(getMessage(V_Sprache, 48) .. string.sub(Player.CompletionCode, 1, 15),
        function()
            Wherigo.Showscreen(Wherigo.MAINSCREEN)
        end)
end

function Auswahl()
    msgMitNachrichtUndBild(getMessage(V_Sprache, 84), P_Signal_traurig,
        function()
            Wherigo.ShowScreen(Wherigo.MAINSCREEN)
        end)
end