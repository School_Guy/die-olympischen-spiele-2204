p1,p2,p3,p4 = 0,0,0,0
EntfernungZahl = 0
EntfernungReal = 0
Richtung = 0
pStart = 0
pEnde = 0
Entfernung1 = 0
Entfernung2 = 0
Entfernung1Real = 0
Entfernung2Real = 0
Entfernung1Zahl = 0
Entfernung2Zahl = 0
d,b = 0,0
walked_distance = 0
entfernungIst = 0
countGPS = 0
countData = 0
GPSOK = false
timerErwuenscht = false
GPSAccuracy = {}
data = {}

function initializeVar (var,vartype)
    if vartype == "number" then
        if var == nil then
            var = 0
        end
    elseif vartype == "boolean" then
        if var == nil then
            var = false
        end
    elseif vartype == "string" then
        if var == nil then
            var = ""
        end
    elseif vartype == "table" then
        if var == nil then
            var = {}
        end
    end
end

function initializeVariables ()
    initializeVar (p1,"number")
    initializeVar (p2,"number")
    initializeVar (p3,"number")
    initializeVar (p4,"number")
    initializeVar (EntfernungZahl,"number")
    initializeVar (EntfernungReal,"number")
    initializeVar (Richtung,"number")
    initializeVar (b,"number")
    initializeVar (pStart,"number")
    initializeVar (pEnde,"number")
    initializeVar (Entfernung1,"number")
    initializeVar (Entfernung2,"number")
    initializeVar (Entfernung1Real,"number")
    initializeVar (Entfernung2Real,"number")
    initializeVar (Entfernung1Zahl,"number")
    initializeVar (Entfernung2Zahl,"number")
    initializeVar (d,"number")
    initializeVar (entfernungIst,"number")
    initializeVar (walked_distance,"number")
    initializeVar (countGPS,"number")
    initializeVar (countData,"number")
    initializeVar (timerErwuenscht,"boolean")
    initializeVar (GPSOK,"boolean")
    initializeVar (GPSAccuracy,"table")
    initializeVar (data,"table")
end

function msgMitNachricht (msg,fun)
    _Urwigo.MessageBox{
        Text = msg,
        Callback = function (action)
            if action ~= nil then
                fun ()
            end
        end
    }
end

function msgMitNachrichtUndBild (msg,med,fun)
    _Urwigo.MessageBox{
        Text = msg,
        Media = med,
        Callback = function (action)
            if action ~= nil then
                fun ()
            end
        end
    }
end

function msgMitNachrichtUndButtonsUndBild (msg,Button1,Button2,med,fun1,fun2)
    _Urwigo.MessageBox{
        Text = msg,
        Media = med,
        Buttons = {
            Button1,
            Button2
        },
        Callback = function(action)
            if action ~= nil then
                if (action == "Button1") == true then
                    fun1()
                elseif (action == "Button2") == true then
                    fun2()
                end
            end
        end
    }
end

function msgMitNachrichtUndButtons (msg,textButton1,textButton2,fun1,fun2)
    _Urwigo.MessageBox{
        Text = msg,
        Buttons = {
            textButton1,
            textButton2
        },
        Callback = function(action)
            if action ~= nil then
                if (action == "Button1") == true then
                    fun1()
                elseif (action == "Button2") == true then
                    fun2()
                end
            end
        end
    }
end

rnd = 0

function getRnd (randomStart,randomStop)
    rnd = math.random (randomStart,randomStop)
    return rnd
end

function TableLength (table)
    local i = 0
    repeat
        i = i + 1
    until table [i] == nil
    i = i - 1
    return i
end

function moveZone (entfernung,zone,pStart,timerErwuenscht)
    --AddLogbookEntry ("Zone bewegen aufgerufen")
    pEnde = Player.ObjectLocation
    getDistance (pStart,pEnde)
    Richtung = b
    EntfernungReal = d:GetValue'm'
    EntfernungZahl = tonumber(d:GetValue'm')
    entfernungIst = entfernungIst + EntfernungZahl
    Entfernung1Zahl = entfernung - entfernungIst + 10
    Entfernung2Zahl = entfernung - entfernungIst
    Entfernung1Real = Wherigo.Distance(Entfernung1Zahl,'m')
    Entfernung2Real = Wherigo.Distance(Entfernung2Zahl,'m')
    if EntfernungZahl <= 5 and timerErwuenscht == true then
        Aktualisierung:Start()
    else
        if Entfernung1Zahl <= 0 or Entfernung2Zahl <= 0 or entfernungIst >= entfernung then
            Entfernung1Zahl = 15
            Entfernung2Zahl = 5
            Entfernung1Real = Wherigo.Distance(Entfernung1Zahl,'m')
            Entfernung2Real = Wherigo.Distance(Entfernung2Zahl,'m')
        end
        p1 = Wherigo.TranslatePoint(pEnde,Entfernung2Real,(Richtung + 40) % 360)
        p2 = Wherigo.TranslatePoint(pEnde,Entfernung1Real,Richtung)
        p3 = Wherigo.TranslatePoint(pEnde,Entfernung2Real,(Richtung - 40) % 360)
        p4 = Wherigo.TranslatePoint(pEnde,Entfernung2Real,Richtung)
        refreshZone (zone,p1,p2,p3,p4)
        if timerErwuenscht == true then
            Aktualisierung:Start()
        end
    end
end

function setZone (zone,pStart,bearing)
    p1 = Wherigo.TranslatePoint(pStart,Wherigo.Distance(50,'m'),bearing)
    p2 = Wherigo.TranslatePoint(pStart,Wherigo.Distance(40,'m'),(bearing + 40) % 360)
    p3 = Wherigo.TranslatePoint(pStart,Wherigo.Distance(40,'m'),bearing)
    p4 = Wherigo.TranslatePoint(pStart,Wherigo.Distance(40,'m'),(bearing - 40) % 360)
    refreshZone (zone,p1,p2,p3,p4)
end

function movePoint (point,referencePoint,distance,bearing)
    point = Wherigo.TranslatePoint(referencePoint,Wherigo.Distance(distance,'m'),bearing)
    return point
end

function refreshZone (zone,p1,p2,p3,p4)
    zone.Points = {
        p1,
        p2,
        p3,
        p4
    }
    zone.Active = false
    zone.Active = true
end

function getDistance (pStart,pEnd)
    d,b = Wherigo.VectorToPoint(pStart, pEnd)
    return d,b
end

function variableZuruecksetzen ()
    entfernungIst = 0
end

function wideZone (zone,bearing)
    local p1old = zone.Points [1]
    local p2old = zone.Points [2]
    local p3old = zone.Points [3]
    local p4old = zone.Points [4]
    p1 = Wherigo.TranslatePoint (p1old,Wherigo.Distance(15,'m'),(bearing - 45) % 360)
    p2 = Wherigo.TranslatePoint (p2old,Wherigo.Distance(15,'m'),(bearing + 45) % 360)
    p3 = Wherigo.TranslatePoint (p3old,Wherigo.Distance(15,'m'),(bearing + 135) % 360)
    p4 = Wherigo.TranslatePoint (p4old,Wherigo.Distance(15,'m'),(bearing + 225) % 360)
    zone.Points = {
        p1,
        p2,
        p3,
        p4
    }
    zone.Active = false
    zone.Active = true
end

function testGPS ()
    --AddLogbookEntry ("GPS Test aufgerufen")
    countGPS = countGPS + 1
    table.insert(GPSAccuracy,countGPS,Player.PositionAccuracy:GetValue('m'))
    O_StartGame.Description = "GPS Genauigkeit ist: " .. GPSAccuracy [countGPS] .. " m" .. cr .. "Du brauchst noch " .. 5 - countGPS .. " Messungen bis zur Ueberpruefung"
    if countGPS == 5 then
        if GPSAccuracy [countGPS - 4] >= 2 and GPSAccuracy [countGPS - 4] < 15 and GPSAccuracy [countGPS - 3] >= 2 and GPSAccuracy [countGPS - 3] < 15 and GPSAccuracy [countGPS - 2] >= 2 and GPSAccuracy [countGPS - 2] < 15 and GPSAccuracy [countGPS - 1] >= 2 and GPSAccuracy [countGPS - 1] < 15 and GPSAccuracy [countGPS] >= 2 and GPSAccuracy [countGPS] < 15 then
            GPSOK = true
            O_StartGame.Description = "GPS ist OK"
            O_StartGame.Commands.Cmd02.Enabled = true
            --AddLogbookEntry ("GPS Test erfolgreich")
        else
            countGPS = 0
            Cd_TestGPS:Start()
        end
    else
        Cd_TestGPS:Start()
    end
end

function Save ()
    --AddLogbookEntry ("Speichern")
    Catridge:RequestSync ()
end

function AddLogbookEntry (entry)
    Logbook.Description = Logbook.Description .. [[

    ]] .. entry
end

function printVersion ()
    print(_VERSION)
end

function computeSaveCode (stationNumber)
    local name = Player.Name
    local res = 0
    local nameSum = 0

    for i = 1,name:len(),1 do
        nameSum = nameSum + string.byte(name,i) * 5
    end
    res = nameSum + stationNumber * 56
    return res
end

--info = debug.getinfo(1,'S');
--info = string.gsub(info.source,"@","")
--filePath = string.sub(info,1,#info-11)
--package.path = package.path..';' .. filePath..'\\luasocket-2.0.2\\lua\\?.lua'
--package.cpath = package.cpath..';' .. filePath..'\\luasocket-2.0.2\\?.dll;' .. filePath..'\\luasocket-2.0.2\\mime\\?.dll'
--print(filePath);
--print(package.path)
--print(package.cpath)
--socket = require("socket")
--print(socket._VERSION)
